FROM ubuntu

MAINTAINER go-clarity

RUN mkdir -p /opt/dev/tomcat/
WORKDIR /opt/dev/tomcat/
RUN apt-get update && apt-get install -y curl
RUN curl --version
RUN curl -O https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.42/bin/apache-tomcat-8.0.42.tar.gz
RUN tar xvfz apache*.tar.gz
RUN ls -altr
RUN apt-get install -y openjdk-8-jdk && apt-get install -y openjdk-8-jre
RUN java -version

WORKDIR /opt/dev/tomcat/apache-tomcat-8.0.42/webapps 
COPY sample.war . 
# RUN curl -O -L https://github.com/AKSarav/SampleWebApp/raw/master/dist/SampleWebApp.war
WORKDIR /opt/dev
RUN apt-get install -y joe && apt-get install -y vim && apt-get install -y net-tools
COPY script.sh .
RUN chmod +x script.sh

ENTRYPOINT ["/opt/dev/script.sh"]
EXPOSE 8080
EXPOSE 8443

# CMD ["/opt/dev/tomcat/apache-tomcat-8.0.42/bin/catalina.sh", "run"]