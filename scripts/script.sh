#!/bin/bash

echo "looking for catalina.sh within opt/dev/..."

TOMCATCATALINA=$(find /opt/dev/ -name "catalina.sh" -type f)
echo $TOMCATCATALINA
if [ -f "$TOMCATCATALINA" ]; then
echo "script found at $TOMCATCATALINA"
    $TOMCATCATALINA version
else
    echo "catalina file not found at /opt/dev/tomcat/apache-tomcat-8.0.42/bin/catalina.sh "
fi

TOMCATUSERSXML=$(find . -name "*tomcat-users.xml" -type f)
LASTLINE=$(awk '/<\/tomcat-users/ {last=NR} END {print last}' $USERSXML)
awk -v lastLine=$LASTLINE '{print $0} NR == lastLine - 1 
{ print "<role rolename=\"marketing-admin\"/>\n <role rolename=\"marketing-viewer\"/>\n<user username=\"admin\" password=\"admin\" roles=\"marketing-admin,marketing-viewer\"/>" }' $USERSXML