#!/bin/bash

declare -A COLOURS
COLOURS=( [SET]='\033[0m' [DARKGRAY]='\033[1;30m' [RED]='\033[0;31m' [WHITE]='\033[1;37m' [LIGHTRED]='\033[1;31m' [GREEN]='\033[0;32m' [PURPLE]='\033[0;35m' [CYAN]='\033[0;36m' [YELLOW]='\033[1;33m' )

echoUpdate () {
    if [ "${COLOURS[${1}]}" ]; then 
           echo -e "${COLOURS[${1}]}$2${COLOURS[SET]}"
        else
        echoUpdate WHITE "${2}"
    fi
}

echoUpdate WHITE "Setting up users ...${SET}"

echoUpdate WHITE "looking for catalina.sh within opt/dev/..."

TOMCATCATALINA=$(find /opt/dev/ -name "catalina.sh" -type f)
echo $TOMCATCATALINA
if [ -f "$TOMCATCATALINA" ]; then
echo "script found at $TOMCATCATALINA"
    $TOMCATCATALINA version
else
    echo "catalina file not found at /opt/dev/tomcat/apache-tomcat-8.0.42/bin/catalina.sh"
fi

echoUpdate WHITE "adding catalina.sh to bin"

ln -s /opt/dev/tomcat/apache-tomcat-8.0.42/bin/catalina.sh  /usr/bin/catalina

echoUpdate WHITE "tomcat's catalina is now availble globally as catalina"

echoUpdate WHITE "updating tomcat-users.xml with the folloowing user and roles"


printf "%-10s %s\n" "Password" "Roles"
echo "===================="
printf "%-10s %s\n" "admin" "marketing-admin, marketing-viewer" 
echo ""

TOMCATUSERSXML=$(find . -name "*tomcat-users.xml" -type f)

read newpassword
LASTLINE=$(awk '/<\/tomcat-users/ {last=NR} END {print last}' $TOMCATUSERSXML)
awk -v lastLine=$LASTLINE '{print $0} NR == lastLine - 1 
{ print "<role rolename=\"marketing-admin\"/>\n <role rolename=\"marketing-viewer\"/>\n<user username=\"admin\" password=\"admin\" roles=\"marketing-admin,marketing-viewer\"/>" }' $TOMCATUSERSXML > $TOMCATUSERSXML.new
mv $TOMCATUSERSXML $TOMCATUSERSXML.bk
mv $TOMCATUSERSXML.new $TOMCATUSERSXML

# keytool -genkey -alias goclarity -keyalg RSA -keystore /opt/dev/gokeystore

keytool -genkey -keystore /root/.keystore -alias ALIAS -keyalg RSA -keysize 4096 -validity 720